let countryData = [];

document.getElementById("back-button").addEventListener("click", () => {
  window.history.back();
});

window.onload = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const countryName = urlParams.get("countryName");

  if (countryName) {
    fetchCountryDetails(countryName);
  } else {
    document.querySelector("#country-details").innerHTML =
      "<p>Country detail is missing ....</p>";
  }
};

async function fetchCountryDetails(countryName) {
  try {
    let response = await fetch("https://restcountries.com/v3.1/all");
    if (response.ok) {
      countryData = await response.json();
      let countryDetail = countryData.filter(
        (country) => country.name.common == countryName
      );
      console.log(countryDetail);
      displayCountryDetails(countryDetail[0]);
    } else {
      document.querySelector("#country-details").innerHTML =
        "<p>Failed to fetch country details.</p>";
    }
  } catch (error) {
    console.error(error);
    document.querySelector("#country-details").innerHTML =
      "<p>Failed to fetch country details.</p>";
  }
}

function displayCountryDetails(country) {
  let nativeNameKey = Object.keys(country.name.nativeName)[0];
  let nativeName = country.name.nativeName[nativeNameKey].common;
  let currencyKeys = Object.keys(country.currencies);
  let currencies = currencyKeys.map((currencyKey) => {
    return country.currencies[currencyKey]["name"];
  });
  currencies = currencies.join(",");

  let languageKeys = Object.keys(country.languages);
  let languages = languageKeys.map((languageKey) => {
    return country.languages[languageKey];
  });
  languages = languages.join(",");

  let borderCountries = [];
  if (country.borders) {
    borderCountries = country.borders
      .map((borderCode) => {
        let borderCountry = countryData.find(
          (country) => country.cca3 === borderCode
        );
        return borderCountry ? borderCountry.name.common : null;
      })
      .filter((name) => name !== null);
  }

  document.querySelector("#country-details").innerHTML = `
        <div class="image-container">
            <img
            src=${country.flags.png}
            class="h-full"
            />
        </div>
        <div class='detail-container'>
            <div class="text-container">
                <h1>${country.name.common}</h1>
                <p>Native Name:<span>${nativeName}</span></p>
                <p>Population:<span>${country.population}</span></p>
                <p>Region:<span>${country.region}</span></p>
                <p>Sub Region:<span>${country.subregion}</span></p> 
                <p>Capital:<span>${country.capital}</span></p>
                <p>Top Level Domain:<span>${country.cca2}</span></p>
                <p>Currencies:<span>${currencies}</span></p>
                <p>Languages:<span>${languages}</span></p>
            </div>
            <div class="border-countries-container ">
              <p>Border Countries :  </p>
              <div class="border-countries-wrapper">
              </div>   
            </div>
        </div>
    `;

  let borderCountriesWrapper = document.querySelector(
    ".border-countries-wrapper"
  );
  borderCountries.forEach((countryName) => {
    let button = document.createElement("button");
    button.appendChild(document.createTextNode(countryName));
    button.className = "country-borders-btn";
    button.addEventListener("click", () => {
      let borderCountry = countryData.filter(
        (country) => country.name.common == countryName
      )[0];
      displayCountryDetails(borderCountry);
    });
    borderCountriesWrapper.appendChild(button);
  });
}
