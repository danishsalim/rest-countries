let cardContainer = document.querySelector(".card-container");
let selectRegion = document.querySelector(".select-region");
let searchBox = document.querySelector(".search-box");
let countriesData = [];
let regions = [];

selectRegion.addEventListener("change", filterByRegion);
searchBox.addEventListener("keyup", searchCountry);

async function fetchingCountriesData() {
  try {
    document.getElementById("loader").style.display = "block";
    let response = await fetch(" https://restcountries.com/v3.1/all");
    if (response.ok) {
      document.getElementById("loader").style.display = "none";
      countriesData = await response.json();
      regionOfEachCountry = countriesData.map((country) => country.region);
      let setOfRegions = new Set(regionOfEachCountry);
      regions = Array.from(setOfRegions);
      regions.sort();
      populateRegionsDropdown();
      displayCountriesData(countriesData);
    } else {
      document.getElementById("loader").style.display = "none";
      let cardContainer = document.querySelector(".card-container");
      cardContainer.innerHTML = " <h2> Error: Failed to fetch data.... </h2>";
      throw new Error("Failed to fetch data");
    }
  } catch (error) {
    console.log(error);
  }
}

function populateRegionsDropdown() {
  selectRegion = document.querySelector(".select-region");
  regions.forEach((region) => {
    let option = document.createElement("option");
    option.value = region;
    option.appendChild(document.createTextNode(region));
    selectRegion.appendChild(option);
  });
}

function filterByRegion(event) {
  let selectedRegion = selectRegion.value;
  let countriesInSelectedRegion = countriesData.filter(
    (country) => country.region == selectedRegion
  );
  displayCountriesData(countriesInSelectedRegion);
}

function searchCountry(event) {
  let inputName = event.target.value.toLowerCase();
  let searchedCountry = [];
  for (let index = 0; index < countriesData.length; index++) {
    if (
      countriesData[index].name.common.toLowerCase().indexOf(inputName) != -1
    ) {
      searchedCountry.push(countriesData[index]);
    }
  }
  displayCountriesData(searchedCountry);
}

function displayCountriesData(countries) {
  let selectReigion = document.querySelector(".select-region");
  let cardContainer = document.querySelector(".card-container");
  cardContainer.innerHTML = " ";
  for (let index = 0; index < countries.length; index++) {
    let card = document.createElement("div");
    card.className = "cards shadow-lg w-52 shadow-lg";
    card.addEventListener("click", () => {
      window.location.href = `detail.html?countryName=${countries[index].name.common}`;
    });
    let imageElement = document.createElement("img");
    imageElement.src = countries[index].flags.png;
    imageElement.className = "card-img";
    card.appendChild(imageElement);
    let textContainer = document.createElement("div");
    textContainer.className = "text-container";
    let h2 = document.createElement("h2");
    h2.appendChild(document.createTextNode(countries[index].name.common));
    textContainer.appendChild(h2);

    let populationPara = document.createElement("p");
    populationPara.appendChild(document.createTextNode("Population: "));
    let populationSpan = document.createElement("span");
    populationSpan.appendChild(
      document.createTextNode(countries[index].population)
    );
    populationPara.appendChild(populationSpan);

    let regionPara = document.createElement("p");
    regionPara.appendChild(document.createTextNode("Region: "));
    let regionSpan = document.createElement("span");
    regionSpan.appendChild(document.createTextNode(countries[index].region));
    regionPara.appendChild(regionSpan);

    let capitalPara = document.createElement("p");
    capitalPara.appendChild(document.createTextNode("Capital: "));
    let capitalSpan = document.createElement("span");
    capitalSpan.appendChild(
      document.createTextNode(...(countries[index].capital || " "))
    );
    capitalPara.appendChild(capitalSpan);

    textContainer.appendChild(populationPara);
    textContainer.appendChild(regionPara);
    textContainer.appendChild(capitalPara);

    card.appendChild(textContainer);
    cardContainer.appendChild(card);
  }
}

window.onload = () => {
  fetchingCountriesData();
};
